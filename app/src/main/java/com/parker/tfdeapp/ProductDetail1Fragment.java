package com.parker.tfdeapp;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductDetail1Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductDetail1Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductDetail1Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProductDetail1Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductDetail1Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductDetail1Fragment newInstance(String param1, String param2) {
        ProductDetail1Fragment fragment = new ProductDetail1Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ProductDetailActivity productdetailactivity = (ProductDetailActivity)getActivity();
        String message = productdetailactivity.getIntent().getStringExtra(ProductCatalogActivity.EXTRA_MESSAGE);
        Product product = productdetailactivity.getSingleProduct(message);

        View view = inflater.inflate(R.layout.fragment_product_detail1,container,false);

        LinkedHashMap<String,String> mapProduct = getProducttoMap(product);

        ImageView imageView = (ImageView) view.findViewById(R.id.productImage);
        imageView.setImageResource(getResources().getIdentifier(product.getImage().get(0),"mipmap",getActivity().getPackageName()));

        ListView listView=(ListView) view.findViewById(R.id.productListview);
        ProductDetailAdapter adapter = new ProductDetailAdapter(getActivity(),mapProduct);
        listView.setAdapter(adapter);

        // Inflate the layout for this fragment
        return view;


    }

    public LinkedHashMap<String,String> getProducttoMap(Product product) {

        LinkedHashMap<String,String> mapProduct = new LinkedHashMap<String,String>();

        //===================================================================================================================
        //Daten aus Product Objekt auslesen und in Hashmap schreiben
        /*
        if(!product.getName().equals("")) {
            mapProduct.put(Name",product.getName());
        }

        if(!product.getPartNo().equals("")) {
            mapProduct.put(Part No",product.getPartNo());
        }
        */
        if(!product.getForm().equals("")) {
            mapProduct.put("Form",product.getForm());
        }

        if(!product.getSeries().equals("")) {
            mapProduct.put("Serie",product.getSeries());
        }

        if(!product.getFittingMaterial().equals("")) {
            mapProduct.put("Verschraubungswerkstoff",product.getFittingMaterial());
        }

        if(!product.getConnector1Style().equals("")) {
            mapProduct.put("Anschlussart 1",product.getConnector1Style());
        }


        if(!product.getConnector2Style().equals("")) {
            mapProduct.put("Anschlussart 2",product.getConnector2Style());
        }

        if(!product.getConnector3Style().equals("")) {
            mapProduct.put("Anschlussart 3",product.getConnector3Style());
        }

        if(product.getTubeOD1()!=0 || product.getTubeOD1()!=null) {
            mapProduct.put("Rohr Außendurchmesser 1",String.valueOf(product.getTubeOD1())+" mm");
        }

        if(product.getTubeOD2()!=null) {
            mapProduct.put("Rohr Außendurchmesser 2",String.valueOf(product.getTubeOD2())+" mm");
        }

        if(product.getTUBEOD3()!=null) {
            mapProduct.put("Rohr Außendurchmesser 3",String.valueOf(product.getTUBEOD3())+" mm");
        }

        if(!product.getPortThread().equals("")) {
            mapProduct.put("Port Gewinde",product.getPortThread());
        }

        if(product.getPN()!=null) {
            mapProduct.put("Druck",String.valueOf(product.getPN())+" bar");
        }

        if(product.getD3()!=null) {
            mapProduct.put("Durchmesser (D3)",String.valueOf(product.getD3())+" mm");
        }

        if(product.getD4()!=null) {
            mapProduct.put("Durchmesser (D4)",String.valueOf(product.getD4())+" mm");
        }

        if(product.getD5()!=null) {
            mapProduct.put("Durchmesser (D5)",String.valueOf(product.getD5())+" mm");
        }

        if(product.getL1()!=null) {
            mapProduct.put("Länge (L1)",String.valueOf(product.getL1())+" mm");
        }

        if(product.getL2()!=null) {
            mapProduct.put("Länge (L2)",String.valueOf(product.getL2())+" mm");
        }

        if(product.getL3()!=null) {
            mapProduct.put("Länge (L3)",String.valueOf(product.getL3())+" mm");
        }

        if(product.getL4()!=null) {
            mapProduct.put("Länge (L4)",String.valueOf(product.getL4())+" mm");
        }

        if(product.getL5()!=null) {
            mapProduct.put("Länge (L5)",String.valueOf(product.getL5())+" mm");
        }

        if(product.getL6()!=null) {
            mapProduct.put("Länge (L6)",String.valueOf(product.getL6())+" mm");
        }

        if(product.getL7()!=null) {
            mapProduct.put("Länge (L7)",String.valueOf(product.getL7())+" mm");
        }

        if(product.getS1()!=null) {
            mapProduct.put("Schlüsselweite (S1)",String.valueOf(product.getS1())+" mm");
        }

        if(product.getS2()!=null) {
            mapProduct.put("Schlüsselweite (S2)",String.valueOf(product.getS2())+" mm");
        }

        if(product.getS3()!=null) {
            mapProduct.put("Schlüsselweite (S3)",String.valueOf(product.getS3())+" mm");
        }

        if(product.getS4()!=null) {
            mapProduct.put("Schlüsselweite (S4)",String.valueOf(product.getS4())+" mm");
        }

        if(product.getS5()!=null) {
            mapProduct.put("Schlüsselweite (S5)",String.valueOf(product.getS5())+" mm");
        }

        if(product.getWeight()!=null) {
            mapProduct.put("Gewicht",String.valueOf(product.getWeight())+" g");
        }
        /*
        if(!product.getImage().equals("")) {
            value+="Image",product.getImage());
        }
        */
        //===================================================================================================================

        return mapProduct;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
