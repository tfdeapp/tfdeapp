package com.parker.tfdeapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        //Übergebenen String als Activity Header setzten
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        this.setTitle(message);

        //Toolbar anzeigen
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_Toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ImageView imageview = (ImageView) findViewById(R.id.imageview_map);
        imageview.setImageResource(R.drawable.tfde_map);

        TextView textview = (TextView) findViewById(R.id.textview_contact);
        textview.setText("PARKER HANNIFIN Manufacturing Germany GmbH & Co. KG\n" +
                "FLUID CONNECTORS GROUP EUROPE\n" +
                "TUBE FITTINGS DIVISION EUROPE\n" +
                "AM METALLWERK 9 \n" +
                "33659 BIELEFELD \n" +
                "GERMANY");

        Button callButton = (Button) findViewById(R.id.button_call);
        Button mailButton = (Button) findViewById(R.id.button_mail);

        mailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"ermeto@parker.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Anfrage");
                startActivity(intent);

            }
        });

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:052140480"));

                startActivity(callIntent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
