package com.parker.tfdeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mbc0158 on 01.02.2017.
 */

public class CustomGrid extends BaseAdapter{
    private Context mContext;
    private final ArrayList<String> gridItemTextList;
    private final ArrayList<Integer> gridItemImageList;

    public CustomGrid(Context c, ArrayList<String> gridItemTextList, ArrayList<Integer> gridItemImageList){
        mContext = c;
        this.gridItemTextList = gridItemTextList;
        this.gridItemImageList = gridItemImageList;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return gridItemTextList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        TextView textView;
        ImageView imageView;

        if (convertView == null) {

            //grid = new View(mContext);
            convertView = inflater.inflate(R.layout.grid_single_main, null);
            textView = (TextView) convertView.findViewById(R.id.grid_single_text);
            imageView = (ImageView) convertView.findViewById(R.id.grid_single_image);
            convertView.setTag(new ViewHolder(textView, imageView));
        } else {

            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            textView = viewHolder.textView;
            imageView = viewHolder.imageView;

        }

        textView.setText(gridItemTextList.get(position));
        imageView.setImageResource(gridItemImageList.get(position));
        return convertView;
    }

    private static class ViewHolder {

        public final TextView textView;
        public final ImageView imageView;

        public ViewHolder(TextView textView, ImageView imageView) {
            this.textView = textView;
            this.imageView = imageView;
        }
    }

}

