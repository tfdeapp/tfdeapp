package com.parker.tfdeapp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class FavouritesActivity extends AppCompatActivity implements FavouritesAdapter.PositiveDeleteListener {

    public final static String EXTRA_MESSAGE = "com.parker.tfdeapp.MESSAGE";
    private FavouritesAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);

        //Übergebenen String als Activity Header setzten
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        this.setTitle(message);

        //Toolbar anzeigen
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_Toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TinyDB sharedPref = new TinyDB(this);
        final ArrayList<String> favoritesList = sharedPref.getListString("productFavorites");

        ListView listview = (ListView)findViewById(R.id.favorites_listview);
        adapter = new FavouritesAdapter(getApplicationContext(),favoritesList,this);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Intent intent = new Intent(FavouritesActivity.this,ProductDetailActivity.class);
                intent.putExtra(EXTRA_MESSAGE, favoritesList.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        TinyDB sharedPref = new TinyDB(this);
        final ArrayList<String> favoritesList = sharedPref.getListString("productFavorites");
        adapter.setItemList(favoritesList);
        adapter.notifyDataSetChanged();
    }

    public void onPositiveDelete(int listPosition){

        TinyDB sharedPref = new TinyDB(this);
        final ArrayList<String> favoritesList = sharedPref.getListString("productFavorites");
        Toast.makeText(this,"Verschraubung "+favoritesList.get(listPosition)+" gelöscht",Toast.LENGTH_SHORT).show();
        favoritesList.remove(listPosition);
        adapter.setItemList(favoritesList);
        adapter.notifyDataSetChanged();
        sharedPref.remove("productFavorites");
        sharedPref.putListString("productFavorites", favoritesList);

    }
    public void onShare(String productName){
        Product product = getSingleProduct(productName);

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_SUBJECT,"Verschraubung "+product.getPartNo());
        String productEmailString = ProductDetailActivity.buildEmailString(product);
        intent.putExtra(Intent.EXTRA_TEXT, productEmailString);

        ArrayList<Bitmap> attachments = new ArrayList<Bitmap>();
        attachments.add(BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(product.getImage().get(0), "mipmap", getPackageName())));
        //attachments.add(BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(product.getImage().get(1), "mipmap", getPackageName())));

        ArrayList<Uri> uris = new ArrayList<Uri>();
        for(Bitmap attachment : attachments) {
            File file = savebitmap(attachment,product);
            uris.add(Uri.fromFile(file));

        }
        intent.putExtra(Intent.EXTRA_STREAM, uris.get(0));

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }

    }
    public File savebitmap(Bitmap bmp, Product product){

        OutputStream outStream = null;
        String path = Environment.getExternalStorageDirectory().toString();
        String filename = product.getPartNo().replaceAll("/","_");
        File file = new File(path,filename+".png");

        int permission_write_external=0;
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, permission_write_external);
        }
        if (file.exists()) {
            file.delete();
            file = new File(path,filename+".png");
        }

        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file;
    }
    public Product getSingleProduct(String PartNr){
        String JSONstring=null;
        Product result = null;
        Gson gson = new Gson();
        Type type = new TypeToken<List<ProductCatalog>>(){}.getType();
        try {
            InputStream is = getAssets().open("data_more.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            JSONstring = new String(buffer, "UTF-8");
        }catch (IOException e){
            e.printStackTrace();
        }

        //Fill Lists for each level
        List<ProductCatalog> ListProductCatalog = gson.fromJson(JSONstring,type);

        //Produktkategorien Liste erstellen
        List<ProductCategory> ListProductCategories = ListProductCatalog.get(0).getProductCategories(); //Liste beinhaltet: Verschraubung,Rohre,
        //Connection Series liste erstellen
        List<ConnectionSeries> ListConnectionSeries = new ArrayList<>();
        for(ProductCategory category : ListProductCategories){

            for(int i=0; i<category.getConnectionSeries().size();i++){
                ListConnectionSeries.add(category.getConnectionSeries().get(i));
            }
        }
        //Produkt liste erstellen
        List<Product> ListProducts = new ArrayList<>();
        for(ConnectionSeries connectionSeries : ListConnectionSeries){

            for(int i=0;i<connectionSeries.getProducts().size();i++)
                ListProducts.add(connectionSeries.getProducts().get(i));
        }

        //Product Objekt finden
        for(int i=0;i<ListProducts.size();i++){
            if(PartNr.equals(ListProducts.get(i).getPartNo())){
                result = ListProducts.get(i);
            }
        }
        return result;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}
