package com.parker.tfdeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class FittingSelectionActivity extends AppCompatActivity implements FittingSelectionDialogFragment.FittingSelectionDialogListener{

    private static final String EXTRA_LEVEL = "com.parker.tfdeapp.LEVEL";
    private static final String EXTRA_MESSAGE = "com.parker.tfdeapp.MESSAGE";
    GridView grid;
    ArrayList<String> filterItems= new ArrayList<String>();
    ArrayList<String>gridItemText= new ArrayList<String>();
    ArrayList<Integer> gridItemImage = new ArrayList<Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitting_selection);

        //GridView Items setzen
        gridItemText.add("Form");
        gridItemText.add("Druck");
        gridItemText.add("Verbindungsart");
        gridItemText.add("Anschlussgröße");
        gridItemText.add("Temperatur");
        gridItemText.add("Medium");


        gridItemImage.add(R.mipmap.fitsel_form);
        gridItemImage.add(R.mipmap.fitsel_pressure);
        gridItemImage.add(R.mipmap.fitsel_connectionseries);
        gridItemImage.add(R.mipmap.fitsel_size);
        gridItemImage.add(R.mipmap.fitsel_temperature);
        gridItemImage.add(R.mipmap.fitsel_medium);


        for(int i=0;i<6;i++){
            filterItems.add(i,"");
        }


        //Übergebenen String als Activity Header setzen
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        String connectionSeries = new String();

        connectionSeries = intent.getStringExtra(ProductCatalogActivity.EXTRA_CONNECTIONSERIES);

        this.setTitle(message);

        //Setze Filter wenn Activity über entsprechende Ebene Produktkatalog gestartet wurde
        if (!TextUtils.isEmpty(connectionSeries)){
            filterItems.set(2,connectionSeries);
            gridItemImage.set(2,R.drawable.ic_checked);
            intent.removeExtra(ProductCatalogActivity.EXTRA_CONNECTIONSERIES);
        }
        //Toolbar anzeigen
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_Toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CustomGrid adapter = new CustomGrid(FittingSelectionActivity.this, gridItemText, gridItemImage);
        grid=(GridView)findViewById(R.id.grid_Main);
        grid.setAdapter(adapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FittingSelectionDialogFragment dialog = new FittingSelectionDialogFragment();
                dialog.setChosenFilterItems(filterItems);
                switch (position){
                    case 0:
                        dialog.setdTitle("Welche Form soll ihre Verschraubung haben?");
                        //Intelligenz für Filterkriterium. Wenn eine Verbindungsart gewählt wurde, wird die Formauswahl eingegrenzt
                        if(filterItems.get(2).equals("Rohr zu Rohr")) {
                            String[] dialoglist = {"Keine Auswahl", "Gerader Stutzen", "Gerade Reduzierung", "T-Stueck", "T-Reduzierung"};
                            dialog.setList(dialoglist);
                        }
                        else if(filterItems.get(2).equals("Einschraubverschraubung")){
                            String[] dialoglist = {"Keine Auswahl","Gerader Einschraubstutzen", "Gerade Einschraubreduzierung"};
                            dialog.setList(dialoglist);
                        }
                        else{
                            String[] dialoglist = {"Keine Auswahl", "Gerader Stutzen", "Gerade Reduzierung", "T-Stueck", "T-Reduzierung", "Gerader Einschraubstutzen", "Gerade Einschraubreduzierung"};
                            dialog.setList(dialoglist);
                        }

                        dialog.setPosition(position);
                        dialog.show(getSupportFragmentManager(),"dialog");
                        break;
                    case 1:
                        dialog.setdTitle("Welchem Druck muss die Verschraubung entsprechen?");
                        dialog.setPosition(position);
                        dialog.show(getSupportFragmentManager(),"dialog");
                        break;
                    case 2:
                        dialog.setdTitle("Was möchten Sie verbinden?");
                        if(filterItems.get(0).equals("Gerader Einschraubstutzen") || filterItems.get(0).equals("Gerade Einschraubreduzierung")) {
                            String[] spinnerlist = {"Keine Auswahl", "Einschraubverschraubung"};
                            dialog.setList(spinnerlist);
                        }
                        else if(filterItems.get(0).equals("Gerader Stutzen") ||filterItems.get(0).equals("Gerade Reduzierung") || filterItems.get(0).equals("T-Stueck") ||filterItems.get(0).equals("T-Reduzierung")){
                            String[] spinnerlist = {"Keine Auswahl", "Rohr zu Rohr"};
                            dialog.setList(spinnerlist);
                        }
                        else{
                            String[] spinnerlist = {"Keine Auswahl", "Rohr zu Rohr", "Einschraubverschraubung"};
                            dialog.setList(spinnerlist);
                        }

                        dialog.setPosition(position);
                        dialog.setCancelable(true);
                        dialog.show(getSupportFragmentManager(),"dialog");
                        break;
                    case 3:
                        dialog.setdTitle("Welche Größe soll ihre Verschraubung haben?");
                        dialog.setPosition(position);
                        String[] sizeList = {"4","6","8","10","12","14","15","16","18","20","22","25","28","30","35","38","42"};
                        dialog.setList(sizeList);
                        dialog.show(getSupportFragmentManager(),"dialog");
                        break;
                    case 4:
                        dialog.setdTitle("Wie hoch ist die max. Betriebstemperatur?");
                        dialog.setPosition(position);
                        dialog.show(getSupportFragmentManager(),"dialog");
                        break;
                    case 5:
                        dialog.setdTitle("Welches Medium wird transportiert?");
                        dialog.setPosition(position);
                        String[] mediumlist ={"Keine Auswahl","Butan","Bremsflüssigkeit","Dieselkraftstoff","Salpetersäure","Phosphorsäure"};
                        dialog.setList(mediumlist);
                        dialog.show(getSupportFragmentManager(),"dialog");
                        break;
                }


            }
        });
        Button filterButton = (Button) findViewById(R.id.button_filter);
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(FittingSelectionActivity.this, ProductCatalogActivity.class);
                intent.putExtra(EXTRA_MESSAGE, "Auswahlergebnis");
                intent.putExtra(EXTRA_LEVEL, "3");
                intent.putStringArrayListExtra("filterList", filterItems);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onChosenDialogItem(String filterString, int position) {

        filterItems.set(position,filterString);
        if (!TextUtils.isEmpty(filterString)) {
            //Toast.makeText(this,filterItems.get(position),Toast.LENGTH_SHORT).show();
            gridItemImage.set(position,R.drawable.ic_checked);
        }
        else{
            //Wenn kein Filter gesetzt wurde, soll das Bild vom Instanzieren wieder gesetzt werden
            switch (position){
                case 0:
                    gridItemImage.set(position, R.mipmap.fitsel_form);
                    break;
                case 1:
                    gridItemImage.set(position, R.mipmap.fitsel_pressure);
                    break;
                case 2:
                    gridItemImage.set(position, R.mipmap.fitsel_connectionseries);
                    break;
                case 3:
                    gridItemImage.set(position, R.mipmap.fitsel_size);
                    break;
                case 4:
                    gridItemImage.set(position, R.mipmap.fitsel_temperature);
                    break;
                case 5:
                    gridItemImage.set(position, R.mipmap.fitsel_medium);
                    break;
            }

        }

        grid.invalidateViews();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds mainactivity_header_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fitting_selection, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.refresh_fittingSelection:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                recreate();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}
