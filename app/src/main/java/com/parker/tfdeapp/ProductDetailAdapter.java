package com.parker.tfdeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.LinkedHashMap;

/**
 * Created by mbc0158 on 16.02.2017.
 */

public class ProductDetailAdapter extends BaseAdapter {

    private LinkedHashMap<String,String> list;
    private Context context;

    public ProductDetailAdapter(Context c, LinkedHashMap<String,String> list){
        super();
        this.context = c;
        this.list=list;

    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ProductDetailAdapter.ViewHolder viewHolder;

        if(convertView == null){
            convertView=inflater.inflate(R.layout.product_detail_data_row,null);

            viewHolder = new ViewHolder();
            viewHolder.textViewKey = (TextView) convertView.findViewById(R.id.productDataKey);
            viewHolder.textViewValue = (TextView) convertView.findViewById(R.id.productDataValue);
            convertView.setTag(viewHolder);

        }else {
            viewHolder = (ProductDetailAdapter.ViewHolder) convertView.getTag();
        }

        //Strings aus Hashmap an "position" auslesen
        viewHolder.textViewKey.setText((String)list.keySet().toArray()[position]);
        viewHolder.textViewValue.setText(list.get(list.keySet().toArray()[position]));

        if(position % 2 == 0){
            viewHolder.textViewKey.setBackgroundColor(context.getResources().getColor(R.color.colorParkerWhite2));
            viewHolder.textViewValue.setBackgroundColor(context.getResources().getColor(R.color.colorParkerWhite2));
        }else{
            viewHolder.textViewKey.setBackgroundColor(context.getResources().getColor(R.color.colorParkerWhite1));
            viewHolder.textViewValue.setBackgroundColor(context.getResources().getColor(R.color.colorParkerWhite1));
        }

        return convertView;
    }

    private static class ViewHolder {

        public TextView textViewKey;
        public TextView textViewValue;

        public ViewHolder(){};

        public ViewHolder(TextView textViewKey, TextView textViewValue) {
            this.textViewKey = textViewKey;
            this.textViewValue = textViewValue;
        }
    }
}
