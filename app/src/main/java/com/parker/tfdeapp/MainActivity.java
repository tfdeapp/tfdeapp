package com.parker.tfdeapp;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.parker.tfdeapp.MESSAGE";
    GridView grid;
    ArrayList<String> gridItemText=new ArrayList<String>();
    ArrayList<Integer> imageID = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_Toolbar);
        setSupportActionBar(toolbar);
        //Header bearbeiten
        ActionBar header = getSupportActionBar();
        //header.setDisplayHomeAsUpEnabled(true);
        //header.setHomeButtonEnabled(true);

        //GridView Items setzen
        gridItemText.add("News");
        gridItemText.add("Produktkatalog");
        gridItemText.add("Produktsuche");
        gridItemText.add("Favoriten");
        gridItemText.add("Verschraubungsauswahl");
        gridItemText.add("Kontakt");

        imageID.add(R.drawable.ic_news2);
        imageID.add(R.drawable.ic_productcatalog);
        imageID.add(R.drawable.ic_search);
        imageID.add(R.drawable.ic_favorites);
        imageID.add(R.drawable.ic_fittingchooser);
        imageID.add(R.drawable.ic_contact1);

        CustomGrid adapter = new CustomGrid(MainActivity.this, gridItemText, imageID);
        grid=(GridView)findViewById(R.id.grid_Main);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(MainActivity.this, "You Clicked at " + gridItemText[+ position], Toast.LENGTH_SHORT).show();

                Intent intent;
                //Je nach Auswahl wird die entsprechende Activity gestartet
                switch (position){
                    case 0:
                        intent = new Intent(MainActivity.this, NewsActivity.class);
                        intent.putExtra(EXTRA_MESSAGE, gridItemText.get(position));
                        startActivity(intent);
                        break;
                    case 1:
                        intent = new Intent(MainActivity.this, ProductCatalogActivity.class);
                        intent.putExtra(EXTRA_MESSAGE, gridItemText.get(position));
                        startActivity(intent);
                        break;
                    case 2:
                        intent = new Intent(MainActivity.this, SearchActivity.class);
                        intent.putExtra(EXTRA_MESSAGE, gridItemText.get(position));
                        startActivity(intent);
                        break;
                    case 3:
                        intent = new Intent(MainActivity.this, FavouritesActivity.class);
                        intent.putExtra(EXTRA_MESSAGE, gridItemText.get(position));
                        startActivity(intent);
                        break;
                    case 4:
                        intent = new Intent(MainActivity.this, FittingSelectionActivity.class);
                        intent.putExtra(EXTRA_MESSAGE, gridItemText.get(position));
                        startActivity(intent);
                        break;
                    case 5:
                        intent = new Intent(MainActivity.this, ContactActivity.class);
                        intent.putExtra(EXTRA_MESSAGE, gridItemText.get(position));
                        startActivity(intent);
                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds mainactivity_header_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.mainactivity_header_items, menu);
        return true;
    }
}
