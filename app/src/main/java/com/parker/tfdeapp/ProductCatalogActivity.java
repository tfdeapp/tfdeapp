package com.parker.tfdeapp;

import android.app.ActivityManager;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ProductCatalogActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.parker.tfdeapp.MESSAGE";
    public final static String EXTRA_LEVEL = "com.parker.tfdeapp.LEVEL";
    public final static String EXTRA_POSITION = "com.parker.tfdeapp.POSITION";
    public final static String EXTRA_CONNECTIONSERIES = "com.parker.tfdeapp.CONNECTIONSERIES";
    int catalogLevel =0;
    GridView gridView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_catalog);

        //Übergebene Strings abfangen
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        final String strCatalogLevel = intent.getStringExtra(ProductCatalogActivity.EXTRA_LEVEL);
        final String strMenuPosition = intent.getStringExtra(ProductCatalogActivity.EXTRA_POSITION);
        final ArrayList<String> filterItems = intent.getStringArrayListExtra("filterList");

        try{
            if(strCatalogLevel!= null)
                catalogLevel = Integer.parseInt(strCatalogLevel);
        }catch(NumberFormatException e){
            catalogLevel=0;
        }

        int menuPosition = 0;
        try {
            if(strMenuPosition != null){
                menuPosition = Integer.parseInt(strMenuPosition);
            }
        }catch (NumberFormatException e){
            menuPosition=0;
        }

        this.setTitle(message);

        //Toolbar anzeigen
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_Toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        //Toast.makeText(ProductCatalogActivity.this, test, Toast.LENGTH_SHORT).show();

        //Set all names of the Gridview Items
        final ArrayList<String> menuData = setmenuData(catalogLevel,menuPosition,false,filterItems);

        //Set all images of the Gridview Items
        final ArrayList<String> menuImage = setmenuData(catalogLevel,menuPosition,true,filterItems);

        final ArrayList<Integer> imagePath = new ArrayList<Integer>();

        for (int i=0; i<menuImage.size();i++){

            //Falls kein Bild für den String im Arrayfeld von menuImage gefunden wird, wird das Bild ic_favourites gesetzt
            try {
                imagePath.add(getResources().getIdentifier(menuImage.get(i), "mipmap", getPackageName()));
                //Falls der String nicht auf eine drawable Ressource matched wird das Bild ic_news gesetzt.
                if(imagePath.get(i)==0){
                    imagePath.add(i,R.drawable.ic_news1);
                }
            }catch (Exception e){
                imagePath.add(i,R.drawable.ic_favorites);
            }
        }

        CustomGrid adapter = new CustomGrid(ProductCatalogActivity.this, menuData, imagePath);


        gridView1=(GridView)findViewById(R.id.grid_Main);
        gridView1.setAdapter(adapter);

        if(menuData.size()==0){
            TextView textview = (TextView) findViewById(R.id.catalogTextview);
            textview.setText("Ihre Auswahl ergab keine Treffer");
            textview.setVisibility(View.VISIBLE);

        }
        gridView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {


                if(catalogLevel>1){
                    Intent intent = new Intent(ProductCatalogActivity.this,ProductDetailActivity.class);
                    intent.putExtra(EXTRA_MESSAGE, menuData.get(position));
                    startActivity(intent);
                }
                else if(catalogLevel==0){
                    if (position == 0) {
                        Intent intent = new Intent(ProductCatalogActivity.this, ProductCatalogActivity.class);
                        intent.putExtra(EXTRA_MESSAGE, menuData.get(position));
                        intent.putExtra(EXTRA_LEVEL, String.valueOf(catalogLevel + 1));
                        intent.putExtra(EXTRA_POSITION, String.valueOf(position));
                        startActivity(intent);
                    }
                }
                else{
                    if (position < 2) {
                        Intent intent = new Intent(ProductCatalogActivity.this, ProductCatalogActivity.class);
                        intent.putExtra(EXTRA_MESSAGE, menuData.get(position));
                        intent.putExtra(EXTRA_LEVEL, String.valueOf(catalogLevel + 1));
                        intent.putExtra(EXTRA_POSITION, String.valueOf(position));
                        startActivity(intent);
                    }
                }
            }
        });

    }

    public ArrayList<String> setmenuData(int level, int position, boolean getImages,ArrayList<String> filterItems){

        int length=0;
        ArrayList<String> TextArray = new ArrayList<String>();
        ArrayList<String> ImageArray= new ArrayList<String>();

        String JSONstring =null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<ProductCatalog>>(){}.getType();
        try {
            InputStream is = getAssets().open("data_more.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            JSONstring = new String(buffer, "UTF-8");
        }catch (IOException e){
            e.printStackTrace();
        }

        //Fill Lists for each level
        List<ProductCatalog> ListProductCatalog = gson.fromJson(JSONstring,type);

        //Produktkategorien Liste erstellen
        List<ProductCategory> ListProductCategories = ListProductCatalog.get(0).getProductCategories(); //Liste beinhaltet: Verschraubung,Rohre,
        //Connection Series liste erstellen
        List<ConnectionSeries> ListConnectionSeries = new ArrayList<>();
        for(ProductCategory category : ListProductCategories){

            for(int i=0; i<category.getConnectionSeries().size();i++){
                ListConnectionSeries.add(category.getConnectionSeries().get(i));
            }
        }
        //Produkt liste erstellen
        List<Product> ListProducts = new ArrayList<>();
        for(ConnectionSeries connectionSeries : ListConnectionSeries){

            for(int i=0;i<connectionSeries.getProducts().size();i++)
                ListProducts.add(connectionSeries.getProducts().get(i));
        }

        switch (level){
            case 0:
                for(int i=0; i< ListProductCategories.size();i++){
                    TextArray.add(ListProductCategories.listIterator(i).next().getName());
                    ImageArray.add(ListProductCategories.listIterator(i).next().getImage());
                }
                break;

            case 1: //Liefert alle Connection Series eine ProductCategory als String Array zurück

                for(int i=0; i< ListProductCategories.get(position).getConnectionSeries().size();i++){
                    TextArray.add(ListProductCategories.get(position).getConnectionSeries().listIterator(i).next().getName());
                    ImageArray.add(ListProductCategories.get(position).getConnectionSeries().listIterator(i).next().getImage());
                }
                break;

            case 2: //Liefert alle Produkte einer ConnectionSeries als String Array zurück

                for(int i=0; i<ListConnectionSeries.get(position).getProducts().size();i++){
                    TextArray.add(ListConnectionSeries.get(position).getProducts().listIterator(i).next().getPartNo());
                    ImageArray.add(ListConnectionSeries.get(position).getProducts().listIterator(i).next().getImage().get(0));
                }
                break;
            case 3://Alle Produkte für Filter selektieren

                for(int i=0; i<ListProducts.size();i++) {
                    if(filterProduct(ListProducts.get(i),filterItems)){
                        TextArray.add(ListProducts.get(i).getPartNo());
                        ImageArray.add(ListProducts.get(i).getImage().get(0));
                    }
                }
                break;
        }

        if (getImages==true){
            return ImageArray;
        }else{
            return TextArray;
        }
    }

    public Boolean filterProduct(Product product,ArrayList<String>filterItems){

        //Filterüberprüfungen

        //Überprüfung Filter Form
        if(!TextUtils.isEmpty(filterItems.get(0))) {
            if (!product.getForm().equals(filterItems.get(0))){
                return false;
            }
        }
        //ÜBerprüfung Filter Druck
        if(!TextUtils.isEmpty(filterItems.get(1))){
            try {
                if (product.getPN() <= Integer.valueOf(filterItems.get(1))) {
                    return false;
                }
            }
            catch (NumberFormatException e){
                if (product.getPN() <= Double.valueOf(filterItems.get(1))) {
                    return false;
                }
            }
        }
        //Überprüfung Filter Verbindungsart
        if(!TextUtils.isEmpty(filterItems.get(2))){
            if(filterItems.get(2).equals("Rohr zu Rohr")) {
                if (!TextUtils.isEmpty(product.getPortThread())) {
                    return false;
                }
            }
            if(filterItems.get(2).equals("Einschraubverschraubung")){
                if (TextUtils.isEmpty(product.getPortThread())) {
                    return false;
                }
            }
        }
        //ÜBerprüfung Filter Anschlussgröße
        if(!TextUtils.isEmpty(filterItems.get(3))){
            if(product.getTubeOD1()!=Integer.valueOf(filterItems.get(3))){
                return false;
            }
        }

        //Überprüfung Filter Temperatur
        if(!TextUtils.isEmpty(filterItems.get(4))){
            if(product.getFittingMaterial().equals("Stahl, Cr(VI)-frei")){
                //Minimale Betriebstemperatur: -40 °C
                //Maximale Betriebstemperatur: 120 °C
                if(Double.valueOf(filterItems.get(4))<-40.0 || Double.valueOf(filterItems.get(4))>120.0){
                    return false;
                }
            }
            if(product.getFittingMaterial().equals("Edelstahl")){
                //Minimale Betriebstemperatur: -60 °C
                //Maximale Betriebstemperatur: 100 °C
                if(Double.valueOf(filterItems.get(4))<-60.0 || Double.valueOf(filterItems.get(4))>100.0){
                    return false;
                }

            }
            if(product.getFittingMaterial().equals("Messing")){
                //Minimale Betriebstemperatur: -60 °C
                //Maximale Betriebstemperatur: 170 °C
                if(Double.valueOf(filterItems.get(4))<-60.0 || Double.valueOf(filterItems.get(4))>170.0){
                    return false;
                }
            }
        }

        //Überprüfung Filter Medium
        if(!TextUtils.isEmpty(filterItems.get(5))){
            //Pro Verschraubungsmaterial müssen nur die unkompatiblen Medien überprüft werden
            // Alle Medien die nicht kompatibel sind geben false zurück
            if(product.getFittingMaterial().equals("Stahl, Cr(VI)-frei")){
                if(filterItems.get(5).equals("Salpetersäure")){
                    return false;
                }
                if(filterItems.get(5).equals("Phosphorsäure")){
                    return false;
                }
            }

            if(product.getFittingMaterial().equals("Edelstahl")){
                if(filterItems.get(5).equals("Phosphorsäure")){
                    return false;
                }
            }

            if(product.getFittingMaterial().equals("Messing")){
                if(filterItems.get(5).equals("Butan")){
                    return false;
                }
                if(filterItems.get(5).equals("Salpetersäure")){
                    return false;
                }
            }

        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds mainactivity_header_items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_catalog, menu);

        //FittingSelection nur an geeigneter Stelle anzeigen
        MenuItem fittingSelection = menu.findItem(R.id.action_fitting_selection);
        MenuItem search = menu.findItem(R.id.action_search);
        if (catalogLevel<1||catalogLevel>2){
            fittingSelection.setVisible(false);
        }
        else{
            fittingSelection.setVisible(true);
        }
        search.setVisible(true);
        /*if(catalogLevel==1){
            search.setVisible(true);
        }
        else{
            search.setVisible(false);
        }*/
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()){
            case R.id.action_search:
                Intent searchIntent = new Intent(ProductCatalogActivity.this,SearchActivity.class);
                searchIntent.putExtra(EXTRA_MESSAGE, "Produktsuche");
                startActivity(searchIntent);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_fitting_selection:
                Intent intent = new Intent(ProductCatalogActivity.this, FittingSelectionActivity.class);
                intent.putExtra(EXTRA_MESSAGE, "Verschraubungsauswahl");
                if(catalogLevel==2){
                    String connectionseries = this.getTitle().toString();
                    intent.putExtra(EXTRA_CONNECTIONSERIES,connectionseries);
                }
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
