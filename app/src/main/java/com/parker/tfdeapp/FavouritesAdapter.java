package com.parker.tfdeapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by mbc0158 on 08.03.2017.
 */

public class FavouritesAdapter extends BaseAdapter {


    private ArrayList<String> itemList;
    private Context mContext;

    //Interface für Kommunikation mit der Activity
    public interface PositiveDeleteListener{
        void onPositiveDelete(int listPosition);
        void onShare(String productname);
    }
    private PositiveDeleteListener mListener;

    public FavouritesAdapter(Context c, ArrayList<String> itemList,Context mlistener){
        this.mContext = c;
        this.itemList = itemList;
        try {
            this.mListener = (PositiveDeleteListener) mlistener;
        }catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement PositiveDeleteListener.");
        }
    }

    public ArrayList<String> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<String> itemList) {
        this.itemList = itemList;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        FavouritesAdapter.ViewHolder viewholder;

        if(convertView==null){
            convertView = inflater.inflate(R.layout.list_item_favorites,null);
            viewholder = new ViewHolder();
            viewholder.textView = (TextView)convertView.findViewById(R.id.textview_favourite_name);
            viewholder.deleteButton = (ImageButton) convertView.findViewById(R.id.button_favorite_delete);
            viewholder.shareButton = (ImageButton) convertView.findViewById(R.id.button_favorite_share);
            convertView.setTag(viewholder);
        }
        else{
            viewholder = (ViewHolder)convertView.getTag();
        }
        //Wenn ein BUtton gedrückt wird, wird die entsprechende INformation an die Activity zurückgeliefert
        viewholder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO Delete Action
                mListener.onPositiveDelete(position);

            }
        });
        viewholder.shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onShare(itemList.get(position));
            }
        });

        viewholder.textView.setText(itemList.get(position));
        return convertView;
    }

    private static class ViewHolder {

        public TextView textView;
        public ImageButton deleteButton;
        public ImageButton shareButton;
        public ViewHolder(){};

        public ViewHolder(TextView textView, ImageButton button) {
            this.textView = textView;
            this.deleteButton = button;

        }
    }


    private AlertDialog AskOption()
    {


        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(mContext)
                //set message, title, and icon
                .setTitle("Löschen")
                .setMessage("Möchten Sie die Verschraubung löschen?")
                .setIcon(R.drawable.ic_trash_filled)

                .setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        //your deleting code
                        dialog.dismiss();
                    }
                })

                .setNegativeButton("Löschen", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }
}
