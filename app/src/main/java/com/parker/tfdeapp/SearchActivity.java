package com.parker.tfdeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "com.parker.tfdeapp.MESSAGE";

    private ArrayList<Product> gridItemProductList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //Übergebenen String als Activity Header setzten
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        this.setTitle(message);

        //Toolbar anzeigen
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_Toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        gridItemProductList = setmenuData();

        final EditText editSearch = (EditText)findViewById(R.id.editText_search);
        editSearch.setHint("Suchbegriff eingeben");
        GridView gridSearch = (GridView)findViewById(R.id.gridview_search);
        final SearchAdapter adapter = new SearchAdapter(this,gridItemProductList);
        gridSearch.setAdapter(adapter);
        gridSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(SearchActivity.this,ProductDetailActivity.class);
                Product temp = (Product)adapter.getItem(position);
                intent.putExtra(EXTRA_MESSAGE, temp.getPartNo());
                startActivity(intent);
            }
        });

        editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                /*
                if(charSequence.toString().substring(count).contains("\n")){
                    charSequence.toString().replace("\n","");
                    return;
                }
                */
                adapter.getFilter().filter(charSequence.toString());

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    public ArrayList<Product> setmenuData() {

        ArrayList<String> TextArray = new ArrayList<String>();
        ArrayList<String> ImageArray = new ArrayList<String>();

        String JSONstring = null;

        Gson gson = new Gson();
        Type type = new TypeToken<List<ProductCatalog>>() {
        }.getType();
        try {
            InputStream is = getAssets().open("data_more.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            JSONstring = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Fill Lists for each level
        ArrayList<ProductCatalog> ListProductCatalog = gson.fromJson(JSONstring, type);

        //Produktkategorien Liste erstellen
        ArrayList<ProductCategory> ListProductCategories = ListProductCatalog.get(0).getProductCategories(); //Liste beinhaltet: Verschraubung,Rohre,
        //Connection Series liste erstellen
        ArrayList<ConnectionSeries> ListConnectionSeries = new ArrayList<>();
        for(ProductCategory category : ListProductCategories){

            for(int i=0; i<category.getConnectionSeries().size();i++){
                ListConnectionSeries.add(category.getConnectionSeries().get(i));
            }
        }
        //Produkt liste erstellen
        ArrayList<Product> ListProducts = new ArrayList<>();
        for(ConnectionSeries connectionSeries : ListConnectionSeries){

            for(int i=0;i<connectionSeries.getProducts().size();i++)
                ListProducts.add(connectionSeries.getProducts().get(i));
        }
        //Alle Produkte in Liste schreiben
        for(int i=0; i<ListProducts.size();i++) {
                TextArray.add(ListProducts.get(i).getPartNo());
                ImageArray.add(ListProducts.get(i).getImage().get(0));
        }
        return ListProducts;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
