package com.parker.tfdeapp;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class NewsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        //Toolbar anzeigen
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_Toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //News aus Intent entnehmen
        Intent intent = getIntent();
        News news = (News)intent.getSerializableExtra("NewsDetail");

        setTitle(news.getTitel());
        //Widgets ansprechen
        ImageView imageview = (ImageView)findViewById(R.id.newsdetail_image);
        //TextView textviewTitle = (TextView)findViewById(R.id.newsDetail_title);
        TextView textviewText = (TextView)findViewById(R.id.newsDetail_text);



        //Widgets mit News INhalt füllen
        imageview.setImageResource(getResources().getIdentifier(news.getImage().toString(),"mipmap", getPackageName()));
        //textviewTitle.setText(news.getTitel());
        textviewText.setText(news.getCompleteText());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
