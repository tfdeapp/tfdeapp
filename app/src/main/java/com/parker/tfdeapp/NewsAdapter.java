package com.parker.tfdeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbc0158 on 13.03.2017.
 */

public class NewsAdapter extends BaseAdapter {

    private ArrayList<News> newsList;
    private Context mContext;

    public NewsAdapter(Context c, ArrayList<News> newsList){
        this.newsList = newsList;
        this.mContext = c;
    }
    @Override
    public int getCount() {
        return newsList.size();
    }

    @Override
    public Object getItem(int position) {
        return newsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewHolder viewholder;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_news, null);
            viewholder = new ViewHolder();
            viewholder.title = (TextView)view.findViewById(R.id.textview_newsTitle);
            viewholder.shortText = (TextView) view.findViewById(R.id.textview_newsShortText);
            viewholder.thumb =(ImageView) view.findViewById(R.id.image_thumb);
            view.setTag(viewholder);
        }
        else {
            viewholder = (ViewHolder)view.getTag();
        }

        viewholder.title.setText(newsList.get(position).getTitel());
        viewholder.shortText.setText(newsList.get(position).getCompleteText().substring(0,65)+"...");
        viewholder.thumb.setImageResource(mContext.getResources().getIdentifier(newsList.get(position).getThumb().toString(),"mipmap", mContext.getPackageName()));
        return view;
    }

    private static class ViewHolder {
        public TextView title;
        public TextView shortText;
        public ImageView thumb;
    }
}
