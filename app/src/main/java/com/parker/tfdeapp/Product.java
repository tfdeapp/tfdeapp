package com.parker.tfdeapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Product {

    @SerializedName("Form")
    @Expose
    private String form;
    @SerializedName("Series")
    @Expose
    private String series;
    @SerializedName("Fitting Material")
    @Expose
    private String fittingMaterial;
    @SerializedName("Connector 1 Style")
    @Expose
    private String connector1Style;
    @SerializedName("Connector 2 Style")
    @Expose
    private String connector2Style;
    @SerializedName("Connector 3 Style")
    @Expose
    private String connector3Style;
    @SerializedName("Tube OD 1")
    @Expose
    private Integer tubeOD1;
    @SerializedName("Tube OD 2 ")
    @Expose
    private Integer tubeOD2;
    @SerializedName("TUBE OD 3")
    @Expose
    private Integer tUBEOD3;
    @SerializedName("Port Thread")
    @Expose
    private String portThread;
    @SerializedName("Part No")
    @Expose
    private String partNo;
    @SerializedName("PN")
    @Expose
    private Integer pN;
    @SerializedName("D3")
    @Expose
    private double d3;
    @SerializedName("D4")
    @Expose
    private Integer d4;
    @SerializedName("D5")
    @Expose
    private Integer d5;
    @SerializedName("L1")
    @Expose
    private Double l1;
    @SerializedName("L2")
    @Expose
    private Double l2;
    @SerializedName("L3")
    @Expose
    private Double l3;
    @SerializedName("L4")
    @Expose
    private Double l4;
    @SerializedName("L5")
    @Expose
    private Double l5;
    @SerializedName("L6")
    @Expose
    private Double l6;
    @SerializedName("L7")
    @Expose
    private Double l7;
    @SerializedName("S1")
    @Expose
    private Integer s1;
    @SerializedName("S2")
    @Expose
    private Integer s2;
    @SerializedName("S3")
    @Expose
    private Integer s3;
    @SerializedName("S4")
    @Expose
    private Integer s4;
    @SerializedName("S5")
    @Expose
    private Integer s5;
    @SerializedName("Weight\ng/1 piece")
    @Expose
    private Integer weight;
    @SerializedName("Image")
    @Expose
    private List<String> image;
    @SerializedName("Name")
    @Expose
    private String name;

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getFittingMaterial() {
        return fittingMaterial;
    }

    public void setFittingMaterial(String fittingMaterial) {
        this.fittingMaterial = fittingMaterial;
    }

    public String getConnector1Style() {
        return connector1Style;
    }

    public void setConnector1Style(String connector1Style) {
        this.connector1Style = connector1Style;
    }

    public String getConnector2Style() {
        return connector2Style;
    }

    public void setConnector2Style(String connector2Style) {
        this.connector2Style = connector2Style;
    }

    public String getConnector3Style() {
        return connector3Style;
    }

    public void setConnector3Style(String connector3Style) {
        this.connector3Style = connector3Style;
    }

    public Integer getTubeOD1() {
        return tubeOD1;
    }

    public void setTubeOD1(Integer tubeOD1) {
        this.tubeOD1 = tubeOD1;
    }

    public Integer getTubeOD2() {
        return tubeOD2;
    }

    public void setTubeOD2(Integer tubeOD2) {
        this.tubeOD2 = tubeOD2;
    }

    public Integer getTUBEOD3() {
        return tUBEOD3;
    }

    public void setTUBEOD3(Integer tUBEOD3) {
        this.tUBEOD3 = tUBEOD3;
    }

    public String getPortThread() {
        return portThread;
    }

    public void setPortThread(String portThread) {
        this.portThread = portThread;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public Integer getPN() {
        return pN;
    }

    public void setPN(Integer pN) {
        this.pN = pN;
    }

    public Double getD3() {
        return d3;
    }

    public void setD3(Double d3) {
        this.d3 = d3;
    }

    public Integer getD4() {
        return d4;
    }

    public void setD4(Integer d4) {
        this.d4 = d4;
    }

    public Integer getD5() {
        return d5;
    }

    public void setD5(Integer d5) {
        this.d5 = d5;
    }

    public Double getL1() {
        return l1;
    }

    public void setL1(Double l1) {
        this.l1 = l1;
    }

    public Double getL2() {
        return l2;
    }

    public void setL2(Double l2) {
        this.l2 = l2;
    }

    public Double getL3() {
        return l3;
    }

    public void setL3(Double l3) {
        this.l3 = l3;
    }

    public Double getL4() {
        return l4;
    }

    public void setL4(Double l4) {
        this.l4 = l4;
    }

    public Double getL5() {
        return l5;
    }

    public void setL5(Double l5) {
        this.l5 = l5;
    }

    public Double getL6() {
        return l6;
    }

    public void setL6(Double l6) {
        this.l6 = l6;
    }

    public Double getL7() {
        return l7;
    }

    public void setL7(Double l7) {
        this.l7 = l7;
    }

    public Integer getS1() {
        return s1;
    }

    public void setS1(Integer s1) {
        this.s1 = s1;
    }

    public Integer getS2() {
        return s2;
    }

    public void setS2(Integer s2) {
        this.s2 = s2;
    }

    public Integer getS3() {
        return s3;
    }

    public void setS3(Integer s3) {
        this.s3 = s3;
    }

    public Integer getS4() {
        return s4;
    }

    public void setS4(Integer s4) {
        this.s4 = s4;
    }

    public Integer getS5() {
        return s5;
    }

    public void setS5(Integer s5) {
        this.s5 = s5;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weightG1Piece) {
        this.weight = weight;
    }

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
