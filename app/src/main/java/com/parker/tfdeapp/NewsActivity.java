package com.parker.tfdeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;

public class NewsActivity extends AppCompatActivity implements Serializable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        //Übergebenen String als Activity Header setzten
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        this.setTitle(message);

        //Toolbar anzeigen
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_Toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //DummyDaten erzeugen
        final News testNews = new News();
        testNews.setTitel("100-jähriges Jubiläum");
        testNews.setCompleteText("Am 13. März 2017 feierte die Parker Hannifin Corporation an allen Standorten sein hundertjähriges Bestehen.\n\nIn TFDE-Headquarter Bielefeld kamen ca. 500 Mitarbeiter zusammen. Sie feierten im Beisein von Oberbürgermeister Pit Clausen und General Manager Achim Kohler dieses nicht alltägliche Jubiläum.");
        //testNews.setShortText(testNews.getCompleteText().substring(0,65)+"...");//("Am 13. März 2017 feierte die Parker Hannifin Corporation an allen Standorten sein 100 jähriges Bestehen.");//("Parker feiert seinen 100. Geburtstag");
        testNews.setImage("news1");
        testNews.setThumb("news1_thumb");
        final ArrayList<News> news = new ArrayList<News>();
        news.add(testNews);

        ListView listview = (ListView)findViewById(R.id.listview_news);
        NewsAdapter adapter = new NewsAdapter(this,news);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(NewsActivity.this,NewsDetailActivity.class);
                intent.putExtra("NewsDetail", (Serializable) testNews);
                startActivity(intent);
            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
