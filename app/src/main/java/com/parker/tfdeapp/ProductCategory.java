package com.parker.tfdeapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductCategory {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("Connection Series")
    @Expose
    private List<ConnectionSeries> connectionSeries = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<ConnectionSeries> getConnectionSeries() {
        return connectionSeries;
    }

    public void setConnectionSeries(List<ConnectionSeries> connectionSeries) {
        this.connectionSeries = connectionSeries;
    }

}
