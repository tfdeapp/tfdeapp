package com.parker.tfdeapp;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

/**
 * Created by mbc0158 on 20.02.2017.
 */

public class FittingSelectionDialogFragment extends DialogFragment {



    private String dTitle;

    private String[] list;

    private int position;

    private String filterItem;

    private List<String> chosenFilterItems;

    public interface FittingSelectionDialogListener {
        public void onChosenDialogItem(String filterStrings,int menuPosition);
    }

    FittingSelectionDialogListener mListener;


    public Dialog onCreateDialog(Bundle savedInstanceState){

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());


        //Erstelle den AlerDialog anhand des gewählten Filters mithilfe der Variable position
        switch (position){
            case 0:
                dialog.setTitle(getdTitle());
                dialog.setItems(getList(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {

                        if(which==0) {
                            filterItem ="";
                        }
                        else{
                            filterItem = list[which];
                        }
                        mListener.onChosenDialogItem(filterItem, position);
                    }
                });
                break;
            case 1:
                final EditText edittext = new EditText(getActivity());
                edittext.setRawInputType(InputType.TYPE_CLASS_NUMBER);

                edittext.requestFocus();
                //Tastatur automatisch mitanzeigen
                final InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,InputMethodManager.HIDE_IMPLICIT_ONLY);
                edittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
                edittext.setPadding(60,0,60,40);
                edittext.setMaxLines(1);
                edittext.setText(chosenFilterItems.get(position));
                edittext.setSelection(edittext.getText().length());
                dialog.setTitle(getdTitle());
                dialog.setMessage("Angaben in PN (bar)");
                dialog.setView(edittext);

                dialog.setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if(TextUtils.isDigitsOnly(edittext.getText().toString())){
                            filterItem = edittext.getText().toString();
                        }
                        else{
                            filterItem="";
                            Toast.makeText(getContext(),"Ungültiger Wert. Die Eingabe für Druck muss eine natürliche Zahl sein.",Toast.LENGTH_LONG).show();
                        }
                        mListener.onChosenDialogItem(filterItem, position);
                        imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
                    }
                });

                dialog.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        filterItem = "";
                        mListener.onChosenDialogItem(filterItem, position);
                        imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
                    }
                });
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
                    }
                });
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
                    }
                });
                dialog.setCancelable(true);

                break;
            case 2:
                final Spinner spinner = new Spinner(getActivity());
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, list);
                spinner.setAdapter((adapter));
                //Wert des Spinners von vorheriger Auswahl übernehmen, falls vorhanden
                for(int i=0; i<list.length;i++){
                    if(list[i].equals(chosenFilterItems.get(position))){
                        spinner.setSelection(i);
                    }
                }
                dialog.setTitle(getdTitle());
                //dialog.setMessage("balbal");
                dialog.setView(spinner);

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        if(i==0){
                            filterItem = "";
                        }
                        else{
                            filterItem = list[i];
                        }
                        //Toast.makeText(getContext(),filterItem,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        filterItem = "";
                    }
                });
                dialog.setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onChosenDialogItem(filterItem,position);
                        dismiss();
                    }
                });
                dialog.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        filterItem = "";
                        mListener.onChosenDialogItem(filterItem, position);
                    }
                });
                break;
            case 3:
                final Spinner spinner2 = new Spinner(getActivity());
                final TextView label = new TextView(getActivity());
                final TextView label2 = new TextView(getActivity());
                LinearLayout layout = new LinearLayout(getActivity());

                layout.setOrientation(LinearLayout.HORIZONTAL);
                layout.setGravity(Gravity.CENTER);

                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, list);
                spinner2.setAdapter(adapter2);

                //Vorauswahl setzen. VOrher gewähltes ListItem durch Überprüfung des values und NICHT der Position
                if(chosenFilterItems.get(position).equals("")) {
                    spinner2.setSelection(0);
                }
                else{
                    for(int i=0; i<list.length;i++){
                        if(list[i].equals(chosenFilterItems.get(position))){
                            spinner2.setSelection(i);
                        }
                    }
                }


                label.setText("Rohr AD:");
                label.setTextColor(getResources().getColor(R.color.colorParkerBlack2));
                label.setTextSize(16);

                label.setPadding(60,0,40,0);

                label2.setText("mm");
                label2.setTextSize(16);
                label2.setTextColor(getResources().getColor(R.color.colorParkerBlack2));

                dialog.setTitle(getdTitle());

                layout.addView(label);
                layout.addView(spinner2);
                layout.addView(label2);

                dialog.setView(layout);

                spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int spinner2Position, long id) {
                        filterItem = list[spinner2Position];
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                dialog.setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        chosenFilterItems.set(position,filterItem);
                        mListener.onChosenDialogItem(filterItem,position);
                        dismiss();
                    }
                });
                dialog.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        filterItem = "";
                        chosenFilterItems.set(position,"");
                        mListener.onChosenDialogItem(filterItem, position);
                    }
                });
                break;
            case 4:

                //Benötigte Widgets instanzieren
                final SeekBar seekbar = new SeekBar(getActivity());
                final TextView labelTemp = new TextView(getActivity());
                LinearLayout layoutTemp = new LinearLayout(getActivity());

                //Wertebereich für Seekbar festlegen
                final int step= 5;
                final int min = -60;
                final int max = 170;
                seekbar.setMax( (max - min) / step);
                seekbar.setPadding(80,60,80,0);

                //Werte der Seekbar, Textview und chosenItemList setzen, wenn kein Filter gesetzt wurde
                if(chosenFilterItems.get(position).equals("")) {
                    seekbar.setProgress(23);
                    labelTemp.setText(String.valueOf(min + (seekbar.getProgress() * step)) + " °C");
                    filterItem = String.valueOf(min + (seekbar.getProgress() * step));
                }
                else{
                    //Wenn bereits ein Filter gesetzt wurde
                    seekbar.setProgress(((Double.valueOf(chosenFilterItems.get(position)).intValue() - min) / step));
                    labelTemp.setText(chosenFilterItems.get(position) + " °C");
                    filterItem =chosenFilterItems.get(position);

                }
                //Textview gestalten und Positionieren
                labelTemp.setTextColor(getResources().getColor(R.color.colorParkerBlack2));
                labelTemp.setTextSize(16);
                labelTemp.setPadding(0,30,20,0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    labelTemp.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }

                //Layout verbinden
                layoutTemp.setOrientation(LinearLayout.VERTICAL);
                layoutTemp.addView(seekbar);
                layoutTemp.addView(labelTemp);

                //Layout an Dialog anheften
                dialog.setTitle(getdTitle());
                dialog.setView(layoutTemp);
                seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                        double value = min + (progress * step);
                        labelTemp.setText(String.valueOf(value)+" °C");
                        filterItem = String.valueOf(value);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
                dialog.setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mListener.onChosenDialogItem(filterItem,position);
                        dismiss();
                    }
                });
                dialog.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        filterItem = "";
                        mListener.onChosenDialogItem(filterItem, position);
                    }
                });


                break;
            case 5:
                dialog.setTitle(getdTitle());
                dialog.setItems(getList(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if(which==0) {
                            filterItem ="";
                        }
                        else{
                            filterItem = list[which];
                        }
                        mListener.onChosenDialogItem(filterItem, position);
                    }
                });
                break;
        }

        return dialog.create();
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        try{

            mListener = (FittingSelectionDialogListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString()+" must implement NoticeDialogListener");
        }
    }

    public String getdTitle() {
        return dTitle;
    }

    public void setdTitle(String dTitle) {
        this.dTitle = dTitle;
    }
    public String[] getList() {
        return list;
    }

    public void setList(String[] list) {
        this.list = list;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<String> getChosenFilterItems() {
        return chosenFilterItems;
    }

    public void setChosenFilterItems(List<String> chosenFilterItems) {
        this.chosenFilterItems = chosenFilterItems;
    }
}
