package com.parker.tfdeapp;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;

import static android.media.CamcorderProfile.get;

public class ProductDetailActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private boolean isFavorite=false;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_Toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        //Übergebene Strings abfangen
        Intent intent = getIntent();
        String message = intent.getStringExtra(ProductCatalogActivity.EXTRA_MESSAGE);
        this.setTitle(message);

        final Product product = getSingleProduct(message);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:"));
                intent.putExtra(Intent.EXTRA_SUBJECT,"Verschraubung "+product.getPartNo());
                String productEmailString = buildEmailString(product);
                intent.putExtra(Intent.EXTRA_TEXT, productEmailString);

                ArrayList<Bitmap> attachments = new ArrayList<Bitmap>();
                attachments.add(BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(product.getImage().get(0), "mipmap", getPackageName())));
                //attachments.add(BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(product.getImage().get(1), "mipmap", getPackageName())));

                ArrayList<Uri> uris = new ArrayList<Uri>();
                for(Bitmap attachment : attachments) {
                    File file = savebitmap(attachment,product);
                    uris.add(Uri.fromFile(file));

                }
                intent.putExtra(Intent.EXTRA_STREAM, uris.get(0));

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });
    }

    public File savebitmap(Bitmap bmp, Product product){

        OutputStream outStream = null;
        String path = Environment.getExternalStorageDirectory().toString();
        String filename = product.getPartNo().replaceAll("/","_");
        File file = new File(path,filename+".png");

        int permission_write_external=0;
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, permission_write_external);
        }
        if (file.exists()) {
            file.delete();
            file = new File(path,filename+".png");
        }

        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file;
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            Toast.makeText(this,"ja",Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }


    public static String buildEmailString(Product product){

        String value=new String();

        if(!product.getForm().equals("")) {
            value+="Form: " +product.getForm()+"\n";
        }

        if(!product.getSeries().equals("")) {
            value+="Serie: "+ product.getSeries()+"\n";
        }

        if(!product.getFittingMaterial().equals("")) {
            value+="Verschraubungswerkstoff: "+ product.getFittingMaterial()+"\n";
        }

        if(!product.getConnector1Style().equals("")) {
            value+="Anschlussart 1: " + product.getConnector1Style()+"\n";
        }


        if(!product.getConnector2Style().equals("")) {
            value+="Anschlussart 2: " + product.getConnector2Style()+"\n";
        }

        if(!product.getConnector3Style().equals("")) {
            value+="Anschlussart 3: " + product.getConnector3Style()+"\n";
        }

        if(product.getTubeOD1()!=0 || product.getTubeOD1()!=null) {
            value+="Rohr Außendurchmesser 1: " + String.valueOf(product.getTubeOD1())+" mm"+"\n";
        }

        if(product.getTubeOD2()!=null) {
            value+="Rohr Außendurchmesser 2: " +String.valueOf(product.getTubeOD2())+" mm"+"\n";
        }

        if(product.getTUBEOD3()!=null) {
            value+="Rohr Außendurchmesser 3: " + String.valueOf(product.getTUBEOD3())+" mm"+"\n";
        }

        if(!product.getPortThread().equals("")) {
            value+="Port Gewinde: " + product.getPortThread()+"\n";
        }

        if(product.getPN()!=null) {
            value+="Druck: " + String.valueOf(product.getPN())+" bar"+"\n";
        }

        if(product.getD3()!=null) {
            value+="D3: "+String.valueOf(product.getD3())+" mm" +"\n";
        }

        if(product.getD4()!=null) {
            value+="D4: "+String.valueOf(product.getD4())+" mm"+"\n";
        }

        if(product.getD5()!=null) {
            value+="D5: "+String.valueOf(product.getD5())+" mm"+"\n";
        }

        if(product.getL1()!=null) {
            value+="L1: "+String.valueOf(product.getL1())+" mm"+"\n";
        }

        if(product.getL2()!=null) {
            value+="L2: "+String.valueOf(product.getL2())+" mm"+"\n";
        }

        if(product.getL3()!=null) {
            value+="L3: "+String.valueOf(product.getL3())+" mm"+"\n";
        }

        if(product.getL4()!=null) {
            value+="L4: "+String.valueOf(product.getL4())+" mm"+"\n";
        }

        if(product.getL5()!=null) {
            value+="L5: "+String.valueOf(product.getL5())+" mm"+"\n";
        }

        if(product.getL6()!=null) {
            value+="L6: "+String.valueOf(product.getL6())+" mm"+"\n";
        }

        if(product.getL7()!=null) {
            value+="L7: "+String.valueOf(product.getL7())+" mm"+"\n";
        }

        if(product.getS1()!=null) {
            value+="S1: "+String.valueOf(product.getS1())+" mm"+"\n";
        }

        if(product.getS2()!=null) {
            value+="S2: "+String.valueOf(product.getS2())+" mm"+"\n";
        }

        if(product.getS3()!=null) {
            value+="S3: "+String.valueOf(product.getS3())+" mm"+"\n";
        }

        if(product.getS4()!=null) {
            value+="S4: "+String.valueOf(product.getS4())+" mm"+"\n";
        }

        if(product.getS5()!=null) {
            value+="S5: "+String.valueOf(product.getS5())+" mm"+"\n";
        }

        if(product.getWeight()!=null) {
            value+="Gewicht: "+String.valueOf(product.getWeight())+" g"+"\n";
        }

        return value;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_detail, menu);

        TinyDB sharedPref = new TinyDB(this);
        ArrayList<String> favorites= new ArrayList<String>();
        MenuItem item = menu.findItem(R.id.action_Favorite);

        favorites.addAll(sharedPref.getListString("productFavorites"));
        //sharedPref.remove("productFavorites");
        for(String product : favorites){
            if(product.equals(getTitle())){
                item.setIcon(R.drawable.ic_favorite_black_24px);
                isFavorite=true;
                break;
            }
            else{
                item.setIcon(R.drawable.ic_favorite_border_black_24px);
                isFavorite=false;
            }
        }
        return true;
    }

    //@TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch(item.getItemId()) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_Favorite:
                TinyDB sharedPref = new TinyDB(this);
                ArrayList<String> favorites = new ArrayList<String>();
                favorites.addAll(sharedPref.getListString("productFavorites"));

                //Bei keinem Element in favorites
                if (favorites.size() == 0) {
                    favorites.add(getTitle().toString());
                    isFavorite=true;
                    item.setIcon(R.drawable.ic_favorite_black_24px);
                    sharedPref.remove("productFavorites");
                    sharedPref.putListString("productFavorites", favorites);
                    Toast.makeText(this,"Zu Favoriten hinzugefügt",Toast.LENGTH_SHORT).show();
                    break;
                }

                if(isFavorite){
                    for (int i = 0; i < favorites.size(); i++) {

                        //Wenn Produkt bereits Favorit ist, dann löschen
                        if (getTitle().toString().equals(favorites.get(i))) {
                            favorites.remove(i);
                            item.setIcon(R.drawable.ic_favorite_border_black_24px);
                            isFavorite=false;
                            Toast.makeText(this,"Favorit gelöscht",Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else if(!isFavorite){
                    for (int i = 0; i < favorites.size(); i++) {

                        //Wenn Produkt noch kein Favorit ist, dann hinzufügen
                        if (!getTitle().toString().equals(favorites.get(i))) {
                            favorites.add(getTitle().toString());
                            item.setIcon(R.drawable.ic_favorite_black_24px);
                            isFavorite=true;
                            Toast.makeText(this,"Zu Favoriten hinzugefügt",Toast.LENGTH_SHORT).show();
                            break;
                        }
                    }
                }
                sharedPref.remove("productFavorites");
                sharedPref.putListString("productFavorites", favorites);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public Product getSingleProduct(String PartNr){
        String JSONstring=null;
        Product result = null;
        Gson gson = new Gson();
        Type type = new TypeToken<List<ProductCatalog>>(){}.getType();
        try {
            InputStream is = getAssets().open("data_more.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            JSONstring = new String(buffer, "UTF-8");
        }catch (IOException e){
            e.printStackTrace();
        }

        //Fill Lists for each level
        List<ProductCatalog> ListProductCatalog = gson.fromJson(JSONstring,type);

        //Produktkategorien Liste erstellen
        List<ProductCategory> ListProductCategories = ListProductCatalog.get(0).getProductCategories(); //Liste beinhaltet: Verschraubung,Rohre,
        //Connection Series liste erstellen
        List<ConnectionSeries> ListConnectionSeries = new ArrayList<>();
        for(ProductCategory category : ListProductCategories){

            for(int i=0; i<category.getConnectionSeries().size();i++){
                ListConnectionSeries.add(category.getConnectionSeries().get(i));
            }
        }
        //Produkt liste erstellen
        List<Product> ListProducts = new ArrayList<>();
        for(ConnectionSeries connectionSeries : ListConnectionSeries){

            for(int i=0;i<connectionSeries.getProducts().size();i++)
                ListProducts.add(connectionSeries.getProducts().get(i));
        }

        //Product Objekt finden
        for(int i=0;i<ListProducts.size();i++){
            if(PartNr.equals(ListProducts.get(i).getPartNo())){
                result = ListProducts.get(i);
            }
        }
        return result;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position){
                case 0:
                    ProductDetail1Fragment tab1 = new ProductDetail1Fragment();
                    return tab1;
                case 1:
                    ProductDetail2Fragment tab2 = new ProductDetail2Fragment();
                    return tab2;
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Daten";
                case 1:
                    return "Bilder";
            }
            return null;
        }
    }
}
