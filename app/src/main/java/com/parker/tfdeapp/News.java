package com.parker.tfdeapp;

import java.io.Serializable;

/**
 * Created by mbc0158 on 13.03.2017.
 */

public class News implements Serializable {

    private String Titel;
    private String shortText;
    private String completeText;
    private String image;
    private String thumb;

    public News() {

    }

    public String getTitel() {
        return Titel;
    }

    public void setTitel(String titel) {
        Titel = titel;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getCompleteText() {
        return completeText;
    }

    public void setCompleteText(String completeText) {
        this.completeText = completeText;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
