package com.parker.tfdeapp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mbc0158 on 09.02.2017.
 */

public class ProductCatalog {

    @SerializedName("ProductCategories")
    @Expose
    private ArrayList<ProductCategory> productCategories = null;

    public ArrayList<ProductCategory> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(ArrayList<ProductCategory> productCategories) {
        this.productCategories = productCategories;
    }

}



