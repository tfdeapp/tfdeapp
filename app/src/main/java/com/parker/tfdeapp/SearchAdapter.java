package com.parker.tfdeapp;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mbc0158 on 10.03.2017.
 */

public class SearchAdapter extends BaseAdapter implements Filterable{
    private Context mContext;
    private ArrayList<Product> gridItemList;
    private ArrayList<Product> originalGridItemList;

    private ProductFilter filter;

    public SearchAdapter(Context c, ArrayList<Product> gridItemList){
        mContext = c;
        this.gridItemList = gridItemList;
        this.originalGridItemList = gridItemList;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return gridItemList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return gridItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public Filter getFilter(){
        if(filter == null){
            filter = new ProductFilter();
        }
        return filter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        TextView textView;
        ImageView imageView;

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.grid_single_main, null);
            textView = (TextView) convertView.findViewById(R.id.grid_single_text);
            imageView = (ImageView) convertView.findViewById(R.id.grid_single_image);
            convertView.setTag(new ViewHolder(textView, imageView));
        } else {

            ViewHolder viewHolder = (ViewHolder) convertView.getTag();
            textView = viewHolder.textView;
            imageView = viewHolder.imageView;

        }

        textView.setText(gridItemList.get(position).getPartNo());
        imageView.setImageResource(mContext.getResources().getIdentifier(gridItemList.get(position).getImage().get(0).toString(),"mipmap", mContext.getPackageName()));
        return convertView;
    }


    private static class ViewHolder {

        public final TextView textView;
        public final ImageView imageView;

        public ViewHolder(TextView textView, ImageView imageView) {
            this.textView = textView;
            this.imageView = imageView;
        }
    }

    private class ProductFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if(!TextUtils.isEmpty(constraint)){
                ArrayList<Product> filteredItems = new ArrayList<Product>();
                ArrayList<Product> outfilteredItems = new ArrayList<Product>();

                for(int i=0; i<originalGridItemList.size();i++){
                    Product product = originalGridItemList.get(i);
                    if(product.getPartNo().toString().toLowerCase().contains(constraint)){
                        filteredItems.add(product);

                    }
                    /*Wenn andere Eigenschaften per Textsuche überprüft werden sollen
                    else if(product.getForm().toString().toLowerCase().contains(constraint)){
                        filteredItems.add(product);
                    }
                    */
                }
                result.values = filteredItems;
                result.count = filteredItems.size();
            }
            else{
                synchronized (this){
                    result.values = originalGridItemList;
                    result.count = originalGridItemList.size();
                }
            }
            return result;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults filterResults) {

            if(filterResults.count== 0){
                gridItemList = (ArrayList<Product>)filterResults.values;
                notifyDataSetChanged();
            }
            else {
                gridItemList = (ArrayList<Product>) filterResults.values;
                notifyDataSetChanged();
            }
        }
    }
}
